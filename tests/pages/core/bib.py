from selenium.webdriver.common.by import By
from pages.core.base import BasePage
from pages.core.components.circulation_widget import CirculationWidget
from pages.core.components.community_activity import CommunityActivity
from pages.core.components.ugc_metadata import UGCMetadata
from pages.core.components.overlay import Overlay

# https://<library>.<environment>.bibliocommons.com/item/show/[ItemID]
class BibPage(BasePage):

    URL_TEMPLATE = "/item/show/{item_id}"   # Usage: BibPage(self.driver, base_url, item_id = [ItemID]).open()

    @property
    def circulation_widget(self):
        return CirculationWidget(self)

    @property
    def community_activity(self):
        return CommunityActivity(self)

    @property
    def ugc_metadata(self):
        return UGCMetadata(self)

    @property
    def overlay(self):
        return Overlay(self)
