from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region

class MyCollectionsComponent(Region):

    _root_locator = (By.CSS_SELECTOR, "[testid='user_dash_my_collections']")
    _heading_locator = (By.CSS_SELECTOR, "[class='panel-heading'] > h4")
    _lists_count_locator = (By.CSS_SELECTOR, "[testid='user_dash_goto_my_lists_with_count'] > h5 > span")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def lists_count(self):
        return self.find_element(*self._lists_count_locator)
