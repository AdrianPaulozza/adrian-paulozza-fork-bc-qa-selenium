from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region
import re

class RecentActivity(Region):

    _root_locator = (By.CLASS_NAME, "cp_activity_feed")
    _feed_event_locator = (By.CLASS_NAME, "feed_event")
    _panel_heading_locator = (By.CLASS_NAME, "panel-heading")   # My Recent Activity

    @property
    def loaded(self):
        try:
            return self.find_element(*self._panel_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    class FeedEvent(Region):
        _message_locator = (By.CLASS_NAME, "feed_message")
        _timestamp_locator = (By.CLASS_NAME, "timestamp")
        _bib_locator = (By.CLASS_NAME, "feed_bib")
        _comment_locator = (By.CLASS_NAME, "feed_item_comment")

        @property
        def message(self):
            return self.find_element(*self._message_locator)

        @property
        def timestamp(self):
            return self.find_element(*self._timestamp_locator)

        @property
        def bib(self):
            return self.find_element(*self._bib_locator)

        @property
        def comment(self):
            # return self.find_element(*self._comment_locator)
            element = self.find_element(*self._comment_locator)
            text = re.sub('^"|"\sPermalink$', '', element.text)
            return text

    @property
    def feed_events(self):
        return [self.FeedEvent(self, element) for element in self.find_elements(*self._feed_event_locator)]
