from selenium.webdriver.common.by import By
# from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from pypom import Region

class Overlay(Region):
    # _root_locator = (By.CLASS_NAME, "add_bib_to_list_overlay")
    # _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
    # # _root_locator = (By.CLASS_NAME, "overlay-inner")
    # # _root_locator = (By.CSS_SELECTOR, "[class='cp-full-screen-overlay simple-genie open']")
    # # _close_icon_locator = (By.CLASS_NAME, "icon-cancel")
    # _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
    # _add_to_list_heading_locator = (By.CSS_SELECTOR, "[class='add_bib_to_list_overlay'] > h2")
    # _list_names_locator = (By.CLASS_NAME, "list_name_column")
    # _create_draft_and_add_locator = (By.CSS_SELECTOR, "a[testid*='add_link_']")
    # _list_actions_locator = (By.CLASS_NAME, "list_actions_column")
    # _only_me_locator = (By.CSS_SELECTOR, "[data-test-id='list-visibility-radio-button-not_visible']")
    # _publish_locator = (By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']")

    class AddToList(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _create_draft_and_add_locator = (By.CSS_SELECTOR, "a[testid*='add_link_']")
        _list_actions_locator = (By.CLASS_NAME, "list_actions_column")
        _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")

        @property
        def loaded(self):
            try:
                # return self.find_element(*self._add_to_list_heading_locator).is_displayed()
                return self.find_element(*self._list_actions_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def create_draft_and_add(self):
            return self.find_elements(*self._create_draft_and_add_locator)

        @property
        def list_actions(self):
            return self.find_elements(*self._list_actions_locator)

        @property
        def close(self):
            return self.find_element(*self._close_locator)

    class ReadyToPublish(Region):
        _root_locator = (By.CSS_SELECTOR, "[class='cp-full-screen-overlay simple-genie open']")
        _only_me_locator = (By.CSS_SELECTOR, "[data-test-id='list-visibility-radio-button-not_visible']")
        _publish_locator = (By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']")
        # _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
        _close_locator = (By.CLASS_NAME, "overlay-close")

        @property
        def loaded(self):
            try:
                # return self.find_element(*self._add_to_list_heading_locator).is_displayed()
                return self.find_element(*self._close_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def only_me(self):
            return self.find_element(*self._only_me_locator)

        @property
        def publish(self):
            return self.find_element(*self._publish_locator)

        @property
        def close(self):
            return self.find_element(*self._close_locator)

    class AddAComment(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
        _edit_comment_locator = (By.ID, "edit_comment")
        _post_comment_locator = (By.NAME, "commit")
        _cancel_locator = (By.CLASS_NAME, "link_cancel")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._close_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def close(self):
            return self.find_element(*self._close_locator)

        @property
        def edit_comment(self):
            return self.find_element(*self._edit_comment_locator)

        @property
        def post_comment(self):
            return self.find_element(*self._post_comment_locator)

        @property
        def cancel(self):
            return self.find_element(*self._cancel_locator)

    class AddTags(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _add_tags_heading_locator = (By.CSS_SELECTOR, "[class='tags_title_wrapper'] > h3")
        _genre_field_locator = (By.CSS_SELECTOR, "[testid='field_tag_genre']")
        _post_tags_locator = (By.NAME, "commit")
        _remove_tags_locator = (By.CSS_SELECTOR, "[data-js='remove_tag']")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._add_tags_heading_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def genre_field(self):
            return self.find_element(*self._genre_field_locator)

        @property
        def post_tags(self):
            return self.find_element(*self._post_tags_locator)

        @property
        def remove_tags(self):
            return self.find_elements(*self._remove_tags_locator)

        @property
        def genre_field(self):
            return self.find_element(*self._genre_field_locator)

    @property
    def ready_to_publish(self):
        return self.ReadyToPublish(self)

    @property
    def add_a_comment(self):
        return self.AddAComment(self)

    @property
    def add_tags(self):
        return self.AddTags(self)

    # @property
    # def loaded(self):
    #     try:
    #         # return self.find_element(*self._add_to_list_heading_locator).is_displayed()
    #         return self.find_element(*self._close_icon_locator).is_displayed()
    #     except NoSuchElementException:
    #         return False

    # @property
    # def close(self):
    #     return self.find_element(*self._close_locator)
    #
    # @property
    # def list_names(self):
    #     return self.find_elements(*self._list_names_locator)
    #
    # @property
    # def create_draft_and_add(self):
    #     return self.find_elements(*self._create_draft_and_add_locator)
    #
    # @property
    # def list_actions(self):
    #     return self.find_elements(*self._list_actions_locator)
    #
    # # "READY TO PUBLISH!" overlay section:
    # @property
    # def only_me(self):
    #     return self.find_element(*self._only_me_locator)
    #
    # @property
    # def publish(self):
    #     return self.find_element(*self._publish_locator)
