from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region

class CirculationWidget(Region):

    _root_locator = (By.CLASS_NAME, "circulation_compact")
    _circulation_stats_locator = (By.CLASS_NAME, "circulation_stats")
    _collection_label_locator = (By.CSS_SELECTOR, "[class='branchInfo'] > div > span[class='label']")
    _collection_value_locator = (By.CSS_SELECTOR, "[class='branchInfo'] > div > span[class='value']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._circulation_stats_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def circulation_stats(self):
        return self.find_element(*self._circulation_stats_locator)

    @property
    def collection_label(self):
        return self.find_element(*self._collection_label_locator)

    @property
    def collection_value(self):
        return self.find_element(*self._collection_value_locator)
