from selenium.webdriver.common.by import By
# from selenium.common.exceptions import NoSuchElementException
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
from .base import BasePage
from pages.core.components.my_collections import MyCollectionsComponent
from pages.core.components.recent_activity import RecentActivity

# https://<library>.<environment>.bibliocommons.com/user_dashboard
class UserDashboardPage(BasePage):

    URL_TEMPLATE = "/user_dashboard"

    @property
    def my_collections(self):
        return MyCollectionsComponent(self)

    @property
    def recent_activity(self):
        return RecentActivity(self)
