from selenium.webdriver.common.by import By
from pages.core.base import BasePage
from pages.core.components.search_result_item import SearchResultItem
import re

# NERF (V2) Page:
# https://<library>.<environment>.bibliocommons.com/v2/search?[&kwargs]
class SearchResultsPage(BasePage):

    _search_result_item_locator = (By.CSS_SELECTOR, "[data-test-id='searchResultItem']")

    @property
    def loaded(self):
        match = re.search(r"(.bibliocommons.com\/v2\/search\?)", self.selenium.current_url)
        if match is not None:
            return True
        else:
            return False

    @property
    def search_result_items(self):
        return [SearchResultItem(self, element) for element in self.find_elements(*self._search_result_item_locator)]
