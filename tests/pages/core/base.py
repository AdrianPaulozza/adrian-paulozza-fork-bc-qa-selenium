from pypom import Page, Region
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time

class BasePage(Page):

    @property
    def header(self):
        return self.Header(self)

    class Header(Region):

        _login_state_user_logged_out_locator = (By.CSS_SELECTOR, "[data-test-id='user_state_logged_out']")
        _log_in_button_locator = (By.CSS_SELECTOR, "[testid='biblionav_login']")
        _login_state_user_logged_in_locator = (By.CLASS_NAME, "logged_in")
        _log_out_locator = (By.CSS_SELECTOR, "[testid='biblionav_logout']")
        _collapsible_search_trigger_locator = (By.CLASS_NAME, "header_collapsible_search_trigger")
        _main_search_input_locator = (By.CSS_SELECTOR, "[testid='main_search_input']")
        # _main_search_button_locator = (By.CSS_SELECTOR, "[type='submit']")
        _advanced_search_locator = (By.CSS_SELECTOR, "[test_id='link_advancedsearch']")
        _my_library_dashboard_locator = (By.CSS_SELECTOR, "[testid='biblionav_mylibrary']")
        _my_collections_lists_locator = (By.CSS_SELECTOR, "[testid='biblionav_lists']")

        # @property
        # def login_state_user_logged_out(self):
        #     return self.find_element(*self._login_state_user_logged_out_locator)
        #
        @property
        def login_state_user_logged_in(self):
            return self.find_element(*self._login_state_user_logged_in_locator)

        @property
        def my_library_dashboard(self):
            return self.find_element(*self._my_library_dashboard_locator)

        @property
        def my_collections_lists(self):
            return self.find_element(*self._my_collections_lists_locator)

        def log_in(self, user_name, password):
            self.find_element(*self._login_state_user_logged_out_locator).click()
            self.find_element(*self._log_in_button_locator).click()
            from pages.core.login import LoginPage
            login_page = LoginPage(self.driver)
            login_page.username_or_barcode.send_keys(user_name)
            login_page.password.send_keys(password)
            login_page.log_in_button.click()
            self.wait.until(lambda s: self.is_element_present(*self._login_state_user_logged_in_locator))

        # @property
        def log_out(self):
            self.find_element(*self._login_state_user_logged_in_locator).click()
            self.find_element(*self._log_out_locator).click()

        @property
        def collapsible_search_trigger(self):
            return self.find_element(*self._collapsible_search_trigger_locator)

        @property
        def main_search_input(self):
            return self.find_element(*self._main_search_input_locator)

        # @property
        # def main_search_button(self):
        #     return self.find_element(*self._main_search_button_locator)

        @property
        def is_collapsible_search_trigger_displayed(self):
            try:
                return self.find_element(*self._collapsible_search_trigger_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def advanced_search(self):
            return self.find_element(*self._advanced_search_locator)

        def search_for(self, term, advanced_search = False):
            # Check if the collapsible search trigger that some libraries have configured
            # e.g (chipublib) is visible and if so click it first:
            if self.is_collapsible_search_trigger_displayed:
                self.collapsible_search_trigger.click()
            # If the 'advanced_search' argument is set to True (default: False), initiate The
            # Advanced Search flow instead:
            if advanced_search:
                self.advanced_search.click()
                from pages.core.search import SearchPage
                search_page = SearchPage(self.driver)
                search_page.search_query.send_keys(" ") # type a single space to trigger the alert
                try:
                    WebDriverWait(self.driver, 3).until(EC.alert_is_present())
                    alert = self.driver.switch_to.alert
                    alert.accept()
                    search_page.search_query.send_keys(term)
                except TimeoutException:
                    print("The 'If you edit the query directly, the form below will be disabled.' alert did NOT appear.")
                search_page.search_button.click()
            else:
                self.main_search_input.send_keys(term, Keys.RETURN)
                # self.main_search_button.click()
            from pages.core.v2.search_results import SearchResultsPage
            search_results_page = SearchResultsPage(self.driver)
            search_results_page.wait_for_page_to_load
            return search_results_page
