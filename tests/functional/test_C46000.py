from selenium import webdriver
import configuration.system
import configuration.chrome
import configuration.user
from mamba import description, it
import sure
from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.bib import BibPage
import re

with description("Core Automation Candidates:") as self:
    with before.all:
        self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        self.driver.quit

    with it("C46000: Go to a UGC-heavy bib page, eg. Twilight"):
        home_page = HomePage(self.driver, "https://chipublib.stage.bibliocommons.com/v2").open()
        search_results_page = home_page.header.search_for("Booked")
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].bib_title.click()
        bib_page = BibPage(self.driver)
        bib_page.wait.until(lambda s: bib_page.circulation_widget.loaded)
        stats = bib_page.circulation_widget.circulation_stats.text.split("\n")
        stats[0].should.match(r"Total Copies: \d+")
        stats[1].should.match(r"Available: \d+")
        stats[2].should.match(r"On Hold: \d+")
        # Get the number of total copies and convert it to an integer:
        total_copies = int(re.search("\d+", stats[0]).group(0))
        # Get the number of on hold copies and convert it to an integer:
        on_hold_copies = int(re.search("\d+", stats[2]).group(0))
        # On hold copies should not exceed 100 times * total copies:
        on_hold_copies.should.be.within(0, (total_copies * 100))
        # Ensure "Collection" is a string that is great than 0 characters:
        bib_page.circulation_widget.collection_value.text.should.be.a(str)
        len(bib_page.circulation_widget.collection_value.text).should.be.greater_than(0)
