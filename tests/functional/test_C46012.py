from selenium import webdriver
import configuration.system
# import configuration.chrome
import configuration.firefox
import configuration.user
from mamba import description, it
import sure
from pages.core.bib import BibPage
from pages.core.user_dashboard import UserDashboardPage
import datetime

with description("Core Automation Candidates:") as self:
    with before.all:
        self.item_id = "719979001"
        # Sigh: https://stackoverflow.com/questions/18483419/selenium-sendkeys-drops-character-with-chrome-driver
        # self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver = webdriver.Firefox(executable_path = configuration.firefox.driver_path)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        # Visit the Bib page and delete the comment added during the test run:
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.community_activity.add_comment.click()
        bib_page.overlay.add_a_comment.edit_comment.clear()
        bib_page.overlay.add_a_comment.post_comment.click()
        self.driver.quit()

    with it("C46012: Add a comment"):
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.header.log_in(configuration.user.name, configuration.user.password)
        bib_page.community_activity.add_comment.click()
        comment_body = "Added by Web Automation [{}]".format(datetime.datetime.now().strftime("%H%M%S%f"))
        bib_page.overlay.add_a_comment.edit_comment.clear()
        bib_page.overlay.add_a_comment.edit_comment.send_keys(comment_body)Ok
        bib_page.overlay.add_a_comment.post_comment.click()
        bib_page.community_activity.comments[0].content.text.should.equal(comment_body)
        user_dashboard_page = UserDashboardPage(self.driver, configuration.system.base_url).open()
        user_dashboard_page.recent_activity.feed_events[0].comment.should.equal(comment_body)
