from selenium import webdriver
import configuration.system
# import configuration.chrome
import configuration.firefox
import configuration.user
from mamba import description, it
import sure
from pages.core.bib import BibPage
import re

with description("Core Automation Candidates:") as self:
    with before.all:
        self.item_id = "719979001"
        # Sigh: https://stackoverflow.com/questions/18483419/selenium-sendkeys-drops-character-with-chrome-driver
        # self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver = webdriver.Firefox(executable_path = configuration.firefox.driver_path)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        # Delete the tag added during the test run:
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.ugc_metadata.add_tags.click()
        bib_page.overlay.add_tags.remove_tags[0].click()
        bib_page.overlay.add_tags.post_tags.click()
        self.driver.quit()

    with it("C47128: Add a tag"):
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.header.log_in(configuration.user.name, configuration.user.password)
        bib_page.ugc_metadata.add_tags.click()
        tag_body = "automagic"
        bib_page.overlay.add_tags.genre_field.send_keys(tag_body)
        bib_page.overlay.add_tags.post_tags.click()
        # Remove the (#) counter from the tag link's text to assert equality:
        tag_link_text = re.sub('\s\(\d+\)', '', bib_page.ugc_metadata.tag_links[0].text)
        tag_body.should.equal(tag_link_text)
