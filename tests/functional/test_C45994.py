from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import configuration.system
import configuration.chrome
from mamba import description, it
import sure

with description("Core Automation Candidates:") as self:
    with before.all:
        self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        self.driver.quit

    with it("C45994: Advanced Search"):
        self.driver.get(configuration.system.base_url)

        advanced_search = self.driver.find_element_by_css_selector("[test_id='link_advancedsearch']")
        advanced_search.click()

        search_query = self.driver.find_element_by_css_selector("[testid='field_boolquery']")
        search_query.send_keys(" ") # type a single space to trigger the alert
        try:
            WebDriverWait(self.driver, 3).until(EC.alert_is_present())
            alert = self.driver.switch_to.alert
            alert.accept()
            search_query.send_keys("Mandela")
        except TimeoutException:
            print("The 'If you edit the query directly, the form below will be disabled.' alert did NOT appear.")
        search_button = self.driver.find_element_by_css_selector("[testid ='button_boolsearch']")
        search_button.click()

        WebDriverWait(self.driver, 15).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "cp-search-result-item-content")))
        search_result_items = self.driver.find_elements_by_class_name("cp-search-result-item-content")
        # Assert that between 1 and 10 (max per page) search results were returned:
        len(search_result_items).should.be.within(1, 10)
