import configuration.chrome
import configuration.user
from mamba import description, it
import sure
from pages.core.home import HomePage

with description("Core Automation Candidates:") as self:
    with before.all:
        self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        self.driver.quit

    with _it("C45997: Select a hold and suspend"):
        home_page = HomePage(self.driver, configuration.system.base_url).open()
