from selenium import webdriver
import configuration.system
import configuration.chrome
import configuration.user
from mamba import description, it
import sure
from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.bib import BibPage
import re
import time

with description("Core Automation Candidates:") as self:
    with before.all:
        self.item_id = "2560434030"
        self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        self.driver.quit

    with _it("C46001: Click all Community Activity tabs"):
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        time.sleep(5)
        comments = bib_page.community_activity_widget.comments
        print(len(comments))
        for comment in comments:
            if comment.is_ratings_displayed:
                print(comment.ratings)
            print(comment.date.text)
            print(comment.user.text)
            print(comment.content.text)
            print(150 * "-")
